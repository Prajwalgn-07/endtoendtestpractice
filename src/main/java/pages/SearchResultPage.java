package pages;

import com.testvagrant.ekam.atoms.web.ElementCollection;
import com.testvagrant.ekam.reports.annotations.WebStep;
import com.testvagrant.ekam.atoms.web.WebPage;
import data.models.Filters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchResultPage extends WebPage {

    private By productsName = query("//*[@class=\"card-information__text h5\"]//a");
    private By availabilityFilter = query("//*[@id=\"FacetsWrapperDesktop\"]/details[1]/summary");
    private By availability = query("//*[@id=\"FacetsWrapperDesktop\"]/details[1]//ul//li//label");
    private By priceFilter = query("//*[@id=\"FacetsWrapperDesktop\"]/details[1]/summary");
    private By lowerLimitPrice = query("//input[@id='Filter-Price-GTE']");
    private By upperLimitPrice = query("//input[@id='Filter-Price-LTE']");
    private By productTypeFilter = query("//*[@id=\"FacetsWrapperDesktop\"]/details[3]/summary]");
    private By productType= query("//*[@id=\"FacetsWrapperDesktop\"]/details[3]//ul//li//label");
    private By brandTypeFilter = query("//*[@id=\"FacetsWrapperDesktop\"]/details[4]/summary]");
    private By brandType= query("//*[@id=\"FacetsWrapperDesktop\"]/details[4]//ul//li//label");
    private By sizeFilter = query("//*[@id=\"FacetsWrapperDesktop\"]/details[5]/summary]");
    private By differentSize= query("//*[@id=\"FacetsWrapperDesktop\"]/details[5]//ul//li//label");

    @WebStep(keyword = "When", description = "I set the filters")
    public SearchResultPage setFilters(Filters filters) {
        setAvailabilityFilter(filters.getAvailability())
                .setProductTypeFilter(filters.getProductType())
                .setBrandTypeFilter(filters.getBrand())
                .setSizeFilter(filters.getSize())
                .setPriceFilter(filters.getPrice().getLowerLimitPrice(),filters.getPrice().getUpperLimitPrice());
        return this;
    }


    @WebStep(keyword = "When", description = "I set the availability Filter")
    public SearchResultPage setAvailabilityFilter(String availabilityType) {
        element(availabilityFilter).click();
        selectFilter(availability,availabilityType);
        return this;
    }

    @WebStep(keyword = "When", description = "I set product type Filter")
    public SearchResultPage setProductTypeFilter(String typeOfProduct) {
        element(productTypeFilter).click();
        selectFilter(productType,typeOfProduct);
        return this;
    }

    @WebStep(keyword = "When", description = "I search for query")
    public SearchResultPage setBrandTypeFilter(String brand) {
        element(brandTypeFilter).click();
        selectFilter(brandType,brand);
        return this;
    }
    @WebStep(keyword = "When", description = "I search for query")
    public SearchResultPage setSizeFilter(String size) {
        element(sizeFilter).click();
        selectFilter(differentSize,size);
        return this;
    }

    @WebStep(keyword = "When", description = "I set the price")
    public SearchResultPage setPriceFilter(String lowestPrice,String highestPrice) {
        element(priceFilter).click();
        textbox(lowerLimitPrice).setText(lowestPrice);
        textbox(upperLimitPrice).setText(highestPrice);
        return this;
    }


    @WebStep(keyword = "When", description = "I get the Names of the product")
    public int countOfProductsContainsProductName(String productName) {
        int productCount=0;
        for(String name:getProductNames())
            if(name.contains(productName))
                productCount++;
        return productCount;
    }

    @WebStep(keyword = "When", description = "I select the filter type")
    public SearchResultPage selectFilter(By by, String filterType) {
        for(WebElement webElement:getListOfFilters(by))
            if(webElement.getText().contains(filterType))
                webElement.click();
        return this;
    }

    @WebStep(keyword = "When", description = " I return the product names")
    public List<String> getProductNames() {
        return new ElementCollection(driver,productsName).getTextValues();
    }

    @WebStep(keyword = "When", description = "I return the list of web elements")
    public List<WebElement> getListOfFilters(By by) {
       return new ElementCollection(driver,by).get();
    }

//    @WebStep(keyword = "When", description = "I select the filter")
//    public SearchResultsPage search(String filter,String filterType) {
//        String filterFormat=String.format("//div[@id='FacetsWrapperDesktop']//span[.='%s']",filter);
//        return this;
//    }

}