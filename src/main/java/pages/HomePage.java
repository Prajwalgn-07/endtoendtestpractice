package pages;

import com.testvagrant.ekam.reports.annotations.WebStep;
import com.testvagrant.ekam.atoms.web.WebPage;
import org.openqa.selenium.By;

public class HomePage extends WebPage {
    private By search = query("//summary[@aria-label='Search']//span//*[name()='svg']");
    private By searchInputField = query("//*[@id=\"Search-In-Modal\"]");
    private By searchButton = query("//*[@class=\"icon icon-search\"]");

    @WebStep(keyword = "When",description = "I select the product")
    public HomePage selectProduct(String productName) {
        String productFormat=String.format("//a[normalize-space()='%s']",productName);
        By productToBuy = query(productFormat);
        element(productToBuy).click();
        return this;
    }

    @WebStep(keyword = "When", description = "I search for product")
    public HomePage search(String product) {
        element(search).click();
        textbox(searchInputField).setText(product);
        element(searchButton).click();
        return this;
    }


}