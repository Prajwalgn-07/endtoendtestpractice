package data.models;

import lombok.Data;

@Data
public class Price {
    private String lowerLimitPrice;
    private String upperLimitPrice;
}
