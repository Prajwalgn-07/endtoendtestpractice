package data.models;

import lombok.Data;

@Data
public class Filters {
	private String availability;
	private String productType;
	private String brand;
	private String size;
	private Price price;
}