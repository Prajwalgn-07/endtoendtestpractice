package data.clients;

import com.testvagrant.ekam.commons.data.DataSetsClient;
import data.models.Filters;

public class FilterDetails extends DataSetsClient {
    public Filters getFilters() {
        return getValue("filters", Filters.class);
    }
}