package data.clients;



import workFlowPattern.UseCase;

import javax.inject.Inject;

public class UseCaseGenerator {
    @Inject CustomerDetails customerDetails;
    @Inject FilterDetails filterDetails;
    @Inject ProductDetails productDetails;
    public workFlowPattern.UseCase happyPathCase(){
        return workFlowPattern.UseCase.builder()
                .build()
                .addToUseCase(new Products(productDetails.getProductSivaJuliet()))
                .addToUseCase(customerDetails.getCustomerWithAccountDetails());
    }
    public workFlowPattern.UseCase MultipleProductsAddingToCartCase(){
        return workFlowPattern.UseCase.builder()
                .build()
                .addToUseCase(new Products(productDetails.getProductLeaceCuff(),productDetails.getProductSivaJuliet()))
                .addToUseCase(customerDetails.getCustomerWithAccountDetails());
    }
}
