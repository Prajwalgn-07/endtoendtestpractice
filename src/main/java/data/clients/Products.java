package data.clients;

import data.models.Product;
import lombok.Getter;

@Getter
public class Products {
    Product productLeaceCuff;
    Product productSivaJuliet;

    public Products(Product productSivaJuliet){
        this.productSivaJuliet=productSivaJuliet;
    }
    public Products(Product productLeaceCuff, Product productSivaJuliet) {
        this.productLeaceCuff = productLeaceCuff;
        this.productSivaJuliet = productSivaJuliet;
    }

}
