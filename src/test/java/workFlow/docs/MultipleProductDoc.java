package workFlow.docs;

import com.testvagrant.ekam.commons.workflows.WorkflowDoc;
import workFlow.definitions.*;
import workFlowPattern.UseCase;
import workFlowPattern.WorkFlowDoc;

public class MultipleProductDoc extends WorkFlowDoc {
    public MultipleProductDoc(UseCase useCase) {
        super(useCase);
    }
    public HomeDefinition selectFirstProduct(){
        return new HomeDefinition(useCase);
    }
    public ProductDefinition product(){
        return selectFirstProduct().selectProductSivaJuliet();
    }
    public HomeDefinition cartFirstProduct(){
        return product().addToCartAndContinueShopping();
    }
    public ProductDefinition selectSecondProduct(){
        return cartFirstProduct().selectProductLeaceCuff();
    }
    public CheckOutDefinition cartSecondProduct(){
        return selectSecondProduct().addToCartAndCheckOut();
    }
    public LoginDefinition checkOut() {
        return cartSecondProduct().next();
    }
    public InformationDefinition login(){
        return checkOut().next();
    }
}