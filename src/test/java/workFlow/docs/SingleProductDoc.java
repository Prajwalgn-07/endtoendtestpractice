package workFlow.docs;

import com.testvagrant.ekam.commons.workflows.WorkflowDoc;
import workFlow.definitions.*;
import workFlowPattern.UseCase;
import workFlowPattern.WorkFlowDoc;


public class SingleProductDoc extends WorkFlowDoc {

    public SingleProductDoc(UseCase useCase) {
        super(useCase);
    }
    public HomeDefinition selectProduct(){
        return new HomeDefinition(useCase);
    }
    public ProductDefinition product(){
        return selectProduct().selectProductSivaJuliet();
    }
    public CheckOutDefinition cart(){
        return product().addToCartAndCheckOut();
    }
    public LoginDefinition checkOut() {
        return cart().next();
    }
    public InformationDefinition login(){
        return checkOut().next();
    }
}
