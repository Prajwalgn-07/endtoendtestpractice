package workFlow.definitions;

import com.testvagrant.ekam.commons.LayoutInitiator;
import com.testvagrant.ekam.commons.workflows.FulfillCondition;
import com.testvagrant.ekam.commons.workflows.WorkflowDefinition;
import pages.ProductPage;
import workFlowPattern.UseCase;
import workFlowPattern.WorkFlowDefinition;


public class ProductDefinition extends WorkFlowDefinition {
    protected FulfillCondition<ProductDefinition> addToCartAndCheckOut=
            ()->{
        create().addToCart().navigateToCart();
        return this;
            };
    protected FulfillCondition<ProductDefinition> addToCartAndContinueShopping=
            ()->{

                create().addToCart().continueShopping().navigateToHomePage();
                return this;
            };

    public ProductDefinition(UseCase useCase){
        super(useCase);
    }
    @Override
    public ProductPage create() {
        return LayoutInitiator.Page(ProductPage.class);
    }

    @Override
    public ProductDefinition next() {
        return this;
    }

    public CheckOutDefinition addToCartAndCheckOut(){
        return proceedToNext(addToCartAndCheckOut,new CheckOutDefinition(useCase));
    }
    public HomeDefinition addToCartAndContinueShopping(){
        return proceedToNext(addToCartAndContinueShopping,new HomeDefinition(useCase));
    }
}
