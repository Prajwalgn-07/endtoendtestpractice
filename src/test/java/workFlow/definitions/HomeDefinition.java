package workFlow.definitions;

import com.testvagrant.ekam.commons.LayoutInitiator;
import com.testvagrant.ekam.commons.workflows.FulfillCondition;
import com.testvagrant.ekam.commons.workflows.WorkflowDefinition;
import data.clients.Products;
import data.models.Product;
import pages.HomePage;
import workFlowPattern.UseCase;
import workFlowPattern.WorkFlowDefinition;


public class HomeDefinition extends WorkFlowDefinition {
    protected FulfillCondition<HomeDefinition> selectProductSivaJuliet=
            ()->{
        create().selectProduct(useCase.getData(Products.class).getProductSivaJuliet().getName());
        return this;
            };
    protected FulfillCondition<HomeDefinition> selectProductLeaceCuff=
            ()->{
                create().selectProduct(useCase.getData(Products.class).getProductLeaceCuff().getName());
                return this;
            };


    public HomeDefinition(UseCase useCase) {
        super(useCase);
    }

    @Override
    public HomePage create() {
        return LayoutInitiator.Page(HomePage.class);
    }

    @Override
    public HomeDefinition next() {
        return this;
    }
    public ProductDefinition selectProductLeaceCuff(){
        return proceedToNext(selectProductLeaceCuff,new ProductDefinition(useCase));
    }
    public ProductDefinition selectProductSivaJuliet() {
        return proceedToNext(selectProductSivaJuliet, new ProductDefinition(useCase));
    }
}
