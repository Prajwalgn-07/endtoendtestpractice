package endToEndTest;

import com.testvagrant.ekam.testBases.testng.WebTest;
import data.clients.CustomerDetails;
import data.clients.ProductDetails;
import data.models.UserDetails;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;

import javax.inject.Inject;

import static com.testvagrant.ekam.commons.LayoutInitiator.Page;

public class PurchaseTests extends WebTest {
    @Inject
    CustomerDetails customerDetails;
    @Inject
    ProductDetails productDetails;

    @Test(groups = "web")
    public void userWithAccountShouldCompletePaymentUsingCod() {
        UserDetails userDetails = customerDetails.getCustomerWithAccountDetails();
        //1. search and select products "sivaJuliet", "leaceCuff"
        //2. Add products "sivaJuliet", "leaceCuff" to cart
        Page(HomePage.class)
                .selectProduct(productDetails.getProductSivaJuliet().getName());
        Page(ProductPage.class)
                .addToCart()
                .continueShopping()
                .navigateToHomePage();
        Page(HomePage.class)
                .selectProduct(productDetails.getProductLeaceCuff().getName());
        //3. Navigate to checkout
        Page(ProductPage.class)
                .addToCart()
                .checkOut();
        //4. User "regular" logs in ->User:{"email":String,"password":String}
        Page(LoginPage.class)
                .fillUserLoginDetails(userDetails)
                .clickOnSignInButton();
        //5. Fill the billing details
        Page(InformationPage.class)
                .enterAddress(userDetails)
                .navigateToShippingPage();
        //6. Navigate to payment
        Page(ShippingPage.class)
                .navigateToPaymentPage();
        //7. Select "cod"
        //8. Verify order details
        Page(PaymentPage.class)
                .selectCod();
        Assert.assertEquals("Payment - ul-web-playground - Checkout",Page(PaymentPage.class).getTitle());
    }
    @Test(groups = "web")
    public void userWithOutAccountShouldCompletePaymentUsingCod() {
        UserDetails userDetails = customerDetails.getCustomerWithoutAccountDetails();
        Page(HomePage.class)
                .selectProduct(productDetails.getProductLeaceCuff().getName());
        Page(ProductPage.class)
                .addToCart()
                .checkOut();
        Page(LoginPage.class)
                .navigateToCreateAccountPage();
        Page(CreateAccountPage.class)
                .fillUserAccountCreationDetails(userDetails)
                .clickCreateButton();
        Page(InformationPage.class)
                .enterAddress(userDetails)
                .navigateToShippingPage();
        Page(ShippingPage.class)
                .navigateToPaymentPage();
        Page(PaymentPage.class)
                .selectCod();
        Assert.assertEquals("Payment - ul-web-playground - Checkout",Page(PaymentPage.class).getTitle());
    }
}
