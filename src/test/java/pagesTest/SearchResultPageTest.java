package pagesTest;

import com.testvagrant.ekam.testBases.testng.WebTest;

import static com.testvagrant.ekam.commons.LayoutInitiator.*;

import data.clients.FilterDetails;
import data.clients.ProductDetails;
import data.models.Filters;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SearchResultPage;

import javax.inject.Inject;

public class SearchResultPageTest extends WebTest {
    @Inject
    FilterDetails filterDetails;
    @Inject
    ProductDetails productDetails;
    Filters filters;
    @BeforeMethod
    public void setup(){
        filters = filterDetails.getFilters();
    }
    @Test(groups = "web")
    public void shouldCheckAllProductsAreJean() {
        Page(HomePage.class)
                .search(productDetails.getProductJean().getName());
        int countOfProduct=Page(SearchResultPage.class)
                                .setAvailabilityFilter(filters.getAvailability())
                                .countOfProductsContainsProductName(productDetails.getProductJean().getName());
        Assert.assertEquals(countOfProduct,10);
    }
    @Test(groups = "web")
    public void shouldCheckAllProductsAreShirt() {
        Page(HomePage.class)
                .search(productDetails.getProductShirts().getName());
        int countOfProduct=Page(SearchResultPage.class)
                                .setAvailabilityFilter(filters.getAvailability())
                                .countOfProductsContainsProductName(productDetails.getProductShirts().getName());
        Assert.assertEquals(countOfProduct,24);
    }

    @Test(groups = "web")
    public void shouldCheckAllProductsAreJeanWithRespectiveFilters() {
        Page(HomePage.class)
                .search(productDetails.getProductJean().getName());
        int countOfProduct=Page(SearchResultPage.class)
                                .setFilters(filters)
                                .countOfProductsContainsProductName(productDetails.getProductJean().getName());
        Assert.assertEquals(countOfProduct,3);
    }


}