package pagesTest;

import data.clients.CustomerDetails;
import data.models.UserDetails;
import pages.HomePage;
import pages.InformationPage;
import pages.LoginPage;
import com.testvagrant.ekam.testBases.testng.WebTest;

import static com.testvagrant.ekam.commons.LayoutInitiator.*;

import org.testng.annotations.Test;
import pages.ProductPage;

import javax.inject.Inject;

public class InformationPageTest extends WebTest {
    @Inject
    CustomerDetails customerDetails;

    @Test(groups = "web")
    public void userCanFillTheShippingDetails() {
        UserDetails userDetails= customerDetails.getCustomerWithAccountDetails();
        Page(HomePage.class)
                .selectProduct("Siva Juliet");
        Page(ProductPage.class)
                .addToCart()
                .checkOut();
        Page(LoginPage.class)
                .fillUserLoginDetails(userDetails)
                .clickOnSignInButton();
        Page(InformationPage.class)
                .enterAddress(userDetails)
                .navigateToShippingPage();
    }
}