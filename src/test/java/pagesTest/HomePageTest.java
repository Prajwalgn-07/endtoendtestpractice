package pagesTest;

import pages.HomePage;
import com.testvagrant.ekam.testBases.testng.WebTest;

import static com.testvagrant.ekam.commons.LayoutInitiator.*;

import org.testng.annotations.Test;

public class HomePageTest extends WebTest {

    @Test(groups = "web")
    public void userCanSelectProductSivaJuliet() {
        Page(HomePage.class)
                .selectProduct("Siva Juliet");
    }

    @Test(groups = "web")
    public void userCanSelectProductLeaceCuff() {
        Page(HomePage.class)
                .selectProduct("Leace Cuff");
    }

    @Test(groups = "web")
    public void shouldReturnSearchedProductResults() {
        Page(HomePage.class)
                .search("Jeans");
    }
}