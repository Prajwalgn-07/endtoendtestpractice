package pagesTest;

import pages.HomePage;
import com.testvagrant.ekam.testBases.testng.WebTest;

import static com.testvagrant.ekam.commons.LayoutInitiator.*;

import org.testng.annotations.Test;
import pages.ProductPage;

public class ProductPageTest extends WebTest {

    @Test(groups = "web")
    public void userCanCheckOutTheProduct() {
        Page(HomePage.class).selectProduct("Siva Juliet");
        Page(ProductPage.class)
                .addToCart()
                .checkOut();
    }

    @Test(groups = "web")
    public void userCanReturnToHomePageAfterAddingToCart() {
        Page(HomePage.class).selectProduct("Leace Cuff");
        Page(ProductPage.class)
                .addToCart()
                .continueShopping()
                .navigateToHomePage();
    }
}